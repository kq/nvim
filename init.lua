require('4ken.settings')
require('4ken.plugins')
require('4ken.maps')

vim.cmd.colorscheme('gruvbox')
vim.cmd.hi('Normal guibg=NONE ctermbg=NONE')
