local function map(mode, lhs, rhs)
	vim.keymap.set(mode, lhs, rhs, { silent = true })
end

local telescopeStatus, telescope = pcall(require, 'telescope.builtin')
if telescopeStatus then
  map('n', '<leader>ff', telescope.find_files)
  map('n', '<leader>fg', telescope.live_grep)
  map('n', '<leader>fb', telescope.buffers)
  map('n', '<leader>fh', telescope.help_tags)
  map('n', '<leader>fs', telescope.git_status)
  map('n', '<leader>fc', telescope.git_commits)
else
  print('Telescope not found')
end
local nvimtreeStatus, api = pcall(require, 'nvim-tree.api')
if nvimtreeStatus then
  map('n', '<leader>e', api.tree.toggle)
  map('n', '<leader>o', api.tree.focus)
  map('n', '<leader>l', ':tabn<CR>')
  map('n', '<leader>h', ':tabp<CR>')
  map('n', '<C-h>', ':wincmd h<CR>')
  map('n', '<C-j>', ':wincmd j<CR>')
  map('n', '<C-k>', ':wincmd k<CR>')
  map('n', '<C-l>', ':wincmd l<CR>')
else
	print('nvim-tree not found')
end

--local dapStatus, dap = pcall(require, 'dap')
--if dapStatus then
--  map('n', '<leader>db', dap.toggle_breakpoint)
--  map('n', '<leader>dr', dap.continue)
--else
--	print('dap not found')
--end

map('n', '<Esc><Esc>', ':noh<CR>')
map('n', '<C-t>', ':tabnew<CR>')
map('t', '<Esc><Esc>', '<C-\\><C-n>')

map('n', '<leader>vt', ':vs | terminal<CR>')
map('n', '<leader>xt', ':18sp | terminal<CR>')

map('n', '<leader>r', '<cmd>lua vim.lsp.buf.rename()<CR>')
map('n', '<leader>d', '<cmd>lua vim.diagnostic.open_float()<CR>')
