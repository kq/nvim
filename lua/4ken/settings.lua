local global = vim.g
local o = vim.o

global.mapleader = ' '
global.maplocalleader = ' '

o.number = true
o.tabstop = 2
o.shiftwidth = 2
o.expandtab = true
o.clipboard = 'unnamedplus'
o.encoding = 'utf-8'
o.updatetime = 100
o.splitright = true
o.splitbelow = true
o.signcolumn = 'yes'
global.loaded_netrw = 1
global.nobackup = true
global.nowritebackup = true
global.loaded_netrwPlugin = 1
