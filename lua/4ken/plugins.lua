return require('packer').startup(function(use)
  use('ellisonleao/gruvbox.nvim')

  use({
    'NeogitOrg/neogit',
    config = function()
      require('neogit').setup({})
    end,
    requires = {
      { 'nvim-lua/plenary.nvim' },
      { 'sindrets/diffview.nvim' },
      {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.8',
        requires = {
          { 'nvim-tree/nvim-web-devicons' },
        },
      },
    },
  })

  use({
    'nvim-tree/nvim-tree.lua',
    config = function()
      require('4ken.configs.ntree')
    end,
    requires = {
      {
        'nvim-tree/nvim-web-devicons',
        config = function()
          require('nvim-web-devicons').refresh({})
        end,
      },
    },
  })

  use({
    'neovim/nvim-lspconfig',
    config = function()
      require('4ken.configs.lsp')
    end,
  })

  use('onsails/lspkind-nvim')

  use({
		'L3MON4D3/LuaSnip',
		tag = 'v2.*',
	})

  use({
		'hrsh7th/nvim-cmp',
		config = function()
			require('4ken.configs.cmp')
		end,
	})

  use('hrsh7th/cmp-nvim-lsp')

	use({
    'hrsh7th/cmp-path',
    after = 'nvim-cmp',
  })

	use({
    'hrsh7th/cmp-buffer',
    after = 'nvim-cmp',
  })

  use({
    'mrcjkb/rustaceanvim',
    tag = '4.26.1',
    config = function()
      require('4ken.configs.rust')
    end
  })

  use({
    'mfussenegger/nvim-lint',
    config = function ()
      require('4ken.configs.lint')
    end
  })

  use({
    'SirVer/ultisnips',
    config = function ()
      require('4ken.configs.ults')
    end
  })

end)
