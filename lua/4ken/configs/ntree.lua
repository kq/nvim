local status, nvimtree = pcall(require, 'nvim-tree')
if not status then
  print('nvim-tree not found')
  return
end

nvimtree.setup({
  sort = {
    sorter = "case_sensitive",
  },
  renderer = {
    group_empty = true,
  },
})
